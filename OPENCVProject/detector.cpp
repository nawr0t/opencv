#include "opencv2/opencv.hpp"
#include <iostream>
#include <sys/stat.h>

using namespace cv;
using namespace std;


void detectAndDisplay(Mat frame);

String cascade = "/Users/Rafal/Desktop/MyPlayground/OPENCV/OPENCVProject/output_cascade/cascade.xml";
String cascade25 = "/Users/Rafal/Desktop/MyPlayground/OPENCV/OPENCVProject/output_cascade25/cascade.xml";

CascadeClassifier cat_cascade;
CascadeClassifier cat_cascade25;

String window_name = "Capture - Cats detection with 17 levels of trainig";
String window_name25 = "Capture - Cats detection with 25 levels of training";


int main(void)
{
  VideoCapture capture;
  VideoCapture capture25;
  Mat frame, frame25;

  if(!cat_cascade.load(cascade)){
    printf("--(!)Error loading first cascade\n");
    return -1; };
  if(!cat_cascade25.load(cascade25)){
    printf("--(!)Error loading second cascade\n");
    return -1; };
  
  capture.open(0);
  capture25.open(0);
  
  if (!capture.isOpened()) {
    printf("--(!)Error opening first video capture\n");
    return -1;}
  if (!capture25.isOpened()) {
    printf("--(!)Error opening second video capture\n");
    return -1;}

  while (capture.read(frame))
  {
    if(frame.empty())
    {
      printf(" --(!) No captured frame -- Break!");
      break;
    }
    detectAndDisplay(frame);
    int c = waitKey(10);
    if((char)c == 27 ) { break; }
  }
  
  while (capture25.read(frame))
  {
    if(frame25.empty())
    {
      printf(" --(!) No captured frame -- Break!");
      break;
    }
    detectAndDisplay(frame25);
    int c = waitKey(10);
    if((char)c == 27) { break; }
  }
  return 0;
}

void detectAndDisplay(Mat frame)
{
  vector<Rect> cats;
  Mat frame_gray;
  cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
  equalizeHist(frame_gray, frame_gray);
  
  /*detectMultiScale(InputArray image, vector<Rect>& objects, double scaleFactor=1.1, int minNeighbors=3, int 
   flags=0, Size minSize=Size(), Size maxSize=Size())*/
  cat_cascade.detectMultiScale(frame_gray, cats, 1.4, 5, 0, Size(20, 20));
  for(size_t i = 0; i < cats.size(); i++)
  {
    Point center(cats[i].x + cats[i].width/2, cats[i].y + cats[i].height/2);
    ellipse( frame, center, Size( cats[i].width/2, cats[i].height/2), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
    Mat faceROI = frame_gray(cats[i]);
  }
  
  imshow(window_name, frame);
  imshow(window_name25, frame);
}
